Class {
	#name : #RgGame,
	#superclass : #Object,
	#instVars : [
		'player',
		'map'
	],
	#category : #'RogueLike-Model'
}

{ #category : #initialization }
RgGame >> initialize [

	super initialize.
	self initializePlayer.
	self initializeMap
]

{ #category : #initialization }
RgGame >> initializeMap [
	
	map := self newMap
]

{ #category : #initialization }
RgGame >> initializePlayer [

	player := RgPlayer new
]

{ #category : #accessing }
RgGame >> map [

	^ map
]

{ #category : #'private factory' }
RgGame >> newMap [

	^ RgMap newGame: self

]

{ #category : #accessing }
RgGame >> player [

	^ player
]

{ #category : #rendering }
RgGame >> renderOn: aConsole [

	self map 
		updateFov: self player;
		renderOn: aConsole
]
