Class {
	#name : #RgInventoryDialog,
	#superclass : #RgConsoleDialog,
	#instVars : [
		'activateAction'
	],
	#category : #'RogueLike-View'
}

{ #category : #actions }
RgInventoryDialog >> activateInventoryItem: aNumber [

	self close.
	activateAction ifNil: [ ^ self ].
	activateAction cull: (self inventory itemAt: aNumber)
]

{ #category : #drawing }
RgInventoryDialog >> drawContentOn: builder [

	builder foregroundColor: Color white.
	self inventory items 
		ifNotEmpty: [ 
			self inventory items withIndexDo: [ :each :index | 
				builder at: 1@index putString: ('({1}) {2}' format: { index. each name }) ] ]
		ifEmpty: [ 
			builder at: 1@1 putString: 'Your inventory is empty.' ]

]

{ #category : #initialization }
RgInventoryDialog >> initialize [

	super initialize.
	self title: 'Inventory'.
	self backgroundColor: Color cyan muchDarker.
	self eventHandler defaultAction: [ :anEvent | 
		anEvent keyCharacter isDigit
			ifTrue: [ self activateInventoryItem: anEvent keyCharacter asString asNumber ] ]
]

{ #category : #drawing }
RgInventoryDialog >> inventory [

	^ self player inventory
]

{ #category : #drawing }
RgInventoryDialog >> player [

	^ self console player
]

{ #category : #enumerating }
RgInventoryDialog >> whenActivatedDo: aBlock [

	activateAction := aBlock
]
