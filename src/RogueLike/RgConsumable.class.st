Class {
	#name : #RgConsumable,
	#superclass : #RgItem,
	#category : #'RogueLike-Model-Items'
}

{ #category : #testing }
RgConsumable class >> isAbstract [

	^ super isAbstract or: [ self = RgConsumable ]
]

{ #category : #testing }
RgConsumable >> isConsumed [

	^ false
]
