Class {
	#name : #RgMapPresenter,
	#superclass : #RgConsolePresenter,
	#instVars : [
		'toTile',
		'fromTile'
	],
	#category : #'RogueLike-View'
}

{ #category : #initialization }
RgMapPresenter >> initialize [

	game := RgGame new.
	super initialize.
	
]

{ #category : #initialization }
RgMapPresenter >> initializePresenters [

	super initializePresenters.
	
	terminal eventHandler whenMouseDownDo: [ :anEvent | 
		self tracePathTo: anEvent position ]
]

{ #category : #private }
RgMapPresenter >> pickFrom: aPoint [
	| position |

	position := self pointToPosition: aPoint.
	position <= self gameMap size ifFalse: [ ^ nil ].

	^ self gameMap tileAt: position
]

{ #category : #private }
RgMapPresenter >> trace [
	
	self updateMap.

	fromTile isWalkable ifFalse: [ ^ self ].	
	toTile isWalkable ifFalse: [ ^ self ].

	self withBuilderDo: [ :builder |	
		builder backgroundColor: Color yellow.
		(RgPlayer new 
			tile: fromTile;
			pathTo: toTile)
			do: [ :each | builder at: each position putCharacter: $* ] ]	
]

{ #category : #private }
RgMapPresenter >> tracePathTo: aPoint [ 

	"reset"
	toTile ifNotNil: [
		fromTile := toTile := nil ].

	self updateMap.

	fromTile ifNil: [
		fromTile := self pickFrom: aPoint.
		^ self ].
	toTile ifNil: [
		toTile := self pickFrom: aPoint.
		self trace ]
]

{ #category : #'private updating' }
RgMapPresenter >> updateMap [

	self application defer: [ 
		terminal 
			clear; 
			hideCursor.
		self gameMap do: [ :each | each renderOn: self ] ]
]
