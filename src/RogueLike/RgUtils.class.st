Class {
	#name : #RgUtils,
	#superclass : #Object,
	#category : #'RogueLike-Utils'
}

{ #category : #private }
RgUtils class >> bresenhamFrom: start to: end [
	| result delta stepx stepy e currentx currenty |
	
	result := Array new writeStream.
	delta := end - start.

	stepx := delta x > 0 
		ifTrue: [ 1 ] 
		ifFalse: [ delta x = 0 ifTrue: [ 0 ]ifFalse: [ -1 ] ].
	stepy := delta y > 0 
		ifTrue: [ 1 ] 
		ifFalse: [ delta y = 0 ifTrue: [ 0 ] ifFalse: [ -1 ] ].
		
	e := ((stepx * delta x) > (stepy * delta y)) 
		ifTrue: [ stepx * delta x ]
		ifFalse: [ stepy * delta y ].
	delta := delta * 2.
	
	currentx := start x.
	currenty := start y.
	result nextPut: start.
	[ (currentx@currenty) = end ] whileFalse: [ 
		(stepx * delta x) > (stepy * delta y) 
			ifTrue: [ 
				currentx := currentx + stepx.
				e := e - (stepy * delta y).
				e < 0 ifTrue: [ 
					currenty := currenty + stepy.
					e := e + (stepx * delta x) ] ]
			ifFalse: [ 
				currenty := currenty + stepy.
				e := e - (stepx * delta x).
				e < 0 ifTrue: [ 
					currentx := currentx + stepx.
					e := e + (stepy * delta y) ] ].
			
			result nextPut: (currentx@currenty) ].
		
	^ result contents
]

{ #category : #'see class side' }
RgUtils >> seeClassSide [
]
