Class {
	#name : #RgConsolePresenter,
	#superclass : #SpPresenter,
	#instVars : [
		'terminal',
		'eventHandlers',
		'game',
		'messages',
		'charSize'
	],
	#category : #'RogueLike-View'
}

{ #category : #'accessing event handler' }
RgConsolePresenter >> activeEventHandler [

	^ eventHandlers last
]

{ #category : #'private ui' }
RgConsolePresenter >> addMessage: aString color: fgColor [
	
	self 
		addMessage: aString 
		foregroundColor: fgColor 
		backgroundColor: Color black "terminal default backgroundColor"
]

{ #category : #'private ui' }
RgConsolePresenter >> addMessage: aString foregroundColor: fgColor backgroundColor: bgColor [

	messages add: { aString. fgColor. bgColor }.
	[ messages size > 4 ] 
	whileTrue: [ messages removeFirst ]
]

{ #category : #private }
RgConsolePresenter >> allowUIToDraw [

	"5 is a magic number we tested is enough to allow the UI to display the spotter"
	5 timesRepeat: [ Processor yield ]
]

{ #category : #accessing }
RgConsolePresenter >> charSize [

	^ charSize ifNil: [ charSize := terminal charSize ]
]

{ #category : #'private ui' }
RgConsolePresenter >> drawWindow: aWindow [
	
	self application defer: [ 
		self withBuilderDo: [ :builder |
			aWindow isCentered 
				ifTrue: [ builder translateTo: ((self terminalSize - aWindow extent) / 2) asIntegerPoint ]
				ifFalse: [ builder translateTo: aWindow position ].
			aWindow drawOn: builder ] ]
		
]

{ #category : #accessing }
RgConsolePresenter >> game [

	^ game ifNil: [ game := self newGame  ]
]

{ #category : #accessing }
RgConsolePresenter >> game: aGame [

	game := aGame
]

{ #category : #accessing }
RgConsolePresenter >> gameMap [
	
	^ self game map
]

{ #category : #'private updating' }
RgConsolePresenter >> handleEnemyTurns [

	self gameMap entities
		reject: [ :each | each isPlayer ] 
		thenDo: [ :each | each playTurn executeOn: self ]
]

{ #category : #'accessing event handler' }
RgConsolePresenter >> handleKeyEvent: anEvent [

	self activeEventHandler handleKeyEvent: anEvent
]

{ #category : #'private events' }
RgConsolePresenter >> handleMouseDownEvent: anEvent [
	| pos tileAtPos path |
	
	pos := self pointToPosition: anEvent position.
	((1@1 corner: self gameMap size) containsPoint: pos) ifFalse: [ ^ self ].
	tileAtPos := self gameMap tileAt: pos.
	tileAtPos isWalkable ifFalse: [ ^ self ].
	
	path := self player pathTo: tileAtPos.
	path ifEmpty: [ ^ self ].
	
	self playPlayerTurn: (RgMovementAction  
		newActor: self player 
		tile: path first)

]

{ #category : #'private events' }
RgConsolePresenter >> handleMouseMoveEvent: anEvent [
]

{ #category : #'private turn' }
RgConsolePresenter >> impossible: aString [

	self addMessage: aString color: Color red.
	self updateMap
]

{ #category : #initialization }
RgConsolePresenter >> initialize [

	super initialize.
	eventHandlers := OrderedCollection new.
	self initializeEventHandler.
	messages := OrderedCollection new
]

{ #category : #initialization }
RgConsolePresenter >> initializeEventHandler [

	self pushEventHandler: (RgKeyboardEventHandler new
		"movement actions"
		bindKeyCombination: Character arrowUp asKeyCombination 		toAction: [ self moveUp ];
		bindKeyCombination: Character arrowDown asKeyCombination 	toAction: [ self moveDown ];
		bindKeyCombination: Character arrowLeft asKeyCombination 	toAction: [ self moveLeft ];
		bindKeyCombination: Character arrowRight asKeyCombination 	toAction: [ self moveRight ];
		bindKeyCombination: Character pageUp asKeyCombination 		toAction: [ self moveRightUp ];
		bindKeyCombination: Character pageDown asKeyCombination 	toAction: [ self moveRightDown ];
		bindKeyCombination: Character home asKeyCombination 			toAction: [ self moveLeftUp ];
		bindKeyCombination: Character end asKeyCombination 			toAction: [ self moveLeftDown ];
		"wait actions"
		bindKeyCombination: KeyboardKey period asKeyCombination 	toAction: [ self wait ];
		"options"
		bindKeyCombination: $i asKeyCombination toAction: [ self showInventoryForAdmin ];
		bindKeyCombination: $u asKeyCombination toAction: [ self showInventoryForUse ];
		bindKeyCombination: $h asKeyCombination toAction: [ self showHelp ];
		yourself)
]

{ #category : #initialization }
RgConsolePresenter >> initializePresenters [

	self layout: (SpBoxLayout newVertical
		add: (terminal := self instantiate: VteTerminalPresenter);
		yourself).
		
	terminal beNotEditable.
	terminal eventHandler 
		whenKeyDownDo: [ :anEvent | self handleKeyEvent: anEvent ];
		whenMouseDownDo: [ :anEvent | self handleMouseDownEvent: anEvent ];
		whenMouseMoveDo: [ :anEvent | self handleMouseMoveEvent: anEvent ].
		
	self 	whenBuiltDo: [
		terminal size: self terminalSize.
		self updateMap ]
]

{ #category : #initialization }
RgConsolePresenter >> initializeWindow: aWindowPresenter [

	aWindowPresenter 
		title: 'Roguelike!';
		initialExtent: 1300@850
]

{ #category : #'actions moving' }
RgConsolePresenter >> moveDown [

	self playPlayerTurnToDelta: (0@ 1)
]

{ #category : #'actions moving' }
RgConsolePresenter >> moveLeft [

	self playPlayerTurnToDelta: (-1 @0)
]

{ #category : #'actions moving' }
RgConsolePresenter >> moveLeftDown [

	self playPlayerTurnToDelta: (-1 @1)
]

{ #category : #'actions moving' }
RgConsolePresenter >> moveLeftUp [

	self playPlayerTurnToDelta: (-1 @ -1)
]

{ #category : #'actions moving' }
RgConsolePresenter >> moveRight [

	self playPlayerTurnToDelta: (1@0)
]

{ #category : #'actions moving' }
RgConsolePresenter >> moveRightDown [

	self playPlayerTurnToDelta: (1@1)
]

{ #category : #'actions moving' }
RgConsolePresenter >> moveRightUp [

	self playPlayerTurnToDelta: (1@ -1)
]

{ #category : #'actions moving' }
RgConsolePresenter >> moveUp [

	self playPlayerTurnToDelta: (0@ -1)
]

{ #category : #'private factory' }
RgConsolePresenter >> newBuilder [

	^ RgConsoleBuilder on: self
]

{ #category : #'private factory' }
RgConsolePresenter >> newDialog [

	^ RgConsoleDialog on: self
]

{ #category : #'private factory' }
RgConsolePresenter >> newGame [

	^ RgGame new
]

{ #category : #'private turn' }
RgConsolePresenter >> newPlayerActionToDelta: aPoint [
	| newPosition |

	newPosition := self player position + aPoint.
	(self gameMap canMoveToPosition: newPosition) 
		ifFalse: [ ^ RgImpossible signal: 'That way is blocked' ].
	
	^ RgBumpAction 
		newActor: self player
		tile: (self gameMap tileAt: newPosition)
]

{ #category : #'private turn' }
RgConsolePresenter >> playAITurn [

	self handleEnemyTurns.
	self updateMap
]

{ #category : #'private updating' }
RgConsolePresenter >> playPlayerIsDead [
	| deadMessage |

	deadMessage := 'You died! (Press ESC key to exit)'.
	self newDialog 
		extent: deadMessage size@1;
		content: [ :builder | 
			builder
				foregroundColor: Color white;
				backgroundColor: Color red;
				at: 1@1 putString: deadMessage ];
		whenClosedDo: [ self window close ];
		show.
]

{ #category : #'private turn' }
RgConsolePresenter >> playPlayerTurn: anAction [ 

	[ 
		anAction executeOn: self.
		self allowUIToDraw. 
		self playAITurn ]
	on: RgImpossible 
	do: [ :e | self impossible: e messageText ]
]

{ #category : #'private turn' }
RgConsolePresenter >> playPlayerTurnToDelta: aPoint [
	
	[ self playPlayerTurn: (self newPlayerActionToDelta: aPoint) ]
	on: RgImpossible 
	do: [ :e | self impossible: e messageText ]
]

{ #category : #accessing }
RgConsolePresenter >> player [

	^ self game player
]

{ #category : #private }
RgConsolePresenter >> pointToPosition: aPoint [

	^ (aPoint / self charSize) asIntegerPoint + (1@1)
]

{ #category : #'private ui' }
RgConsolePresenter >> popDialog: aDialog [

	self popEventHandler.
	self updateMap
]

{ #category : #'accessing event handler' }
RgConsolePresenter >> popEventHandler [

	^ eventHandlers removeLast
]

{ #category : #'private ui' }
RgConsolePresenter >> pushDialog: aDialog [
	
	self pushEventHandler: aDialog eventHandler		
]

{ #category : #'accessing event handler' }
RgConsolePresenter >> pushEventHandler: anEventHandler [

	eventHandlers addLast: anEventHandler
]

{ #category : #rendering }
RgConsolePresenter >> renderEntity: anEntity [
	
	self 
		renderEntity: anEntity 
		tile: (self gameMap tileAt: anEntity position)
]

{ #category : #rendering }
RgConsolePresenter >> renderEntity: anEntity tile: aTile [
	
	self withBuilderDo: [ :builder | 
		builder
			foregroundColor: anEntity color;
			backgroundColor: aTile lightBackgroundColor;  
			at: anEntity position putCharacter: anEntity symbol ]
]

{ #category : #rendering }
RgConsolePresenter >> renderExploredTile: aTile [

	self withBuilderDo: [ :builder |
		builder  
			foregroundColor: aTile darkForegroundColor;
			backgroundColor: aTile darkBackgroundColor;
			at: aTile position putCharacter: aTile darkCharacter ]
]

{ #category : #rendering }
RgConsolePresenter >> renderTile: aTile [

	self renderExploredTile: aTile
]

{ #category : #rendering }
RgConsolePresenter >> renderVisibleTile: aTile [

	self withBuilderDo: [ :builder | 
		builder
			foregroundColor: aTile lightForegroundColor;
			backgroundColor: aTile lightBackgroundColor;
			at: aTile position putCharacter: aTile darkCharacter ] 
]

{ #category : #actions }
RgConsolePresenter >> showHelp [

	(RgConsoleDialog on: self)
		title: 'Help';
		backgroundColor: Color darkGray;
		content: [ :builder | 
			builder 
				at: 1@1 putString: '(i) Manage Inventory';
				at: 1@2 putString: '(u) Use item from Inventory';
				at: 1@3 putString: '---';
				at: 1@4 putString: '(h) Show this help' ];
		show
]

{ #category : #actions }
RgConsolePresenter >> showInventoryForAdmin [

	(RgInventoryDialog on: self) 
		whenActivatedDo: [ :anItem | 
			self playPlayerTurn: (RgDropItemAction new
				actor: self player;
				item: anItem;
				yourself) ];
		show
]

{ #category : #actions }
RgConsolePresenter >> showInventoryForUse [

	(RgInventoryDialog on: self)
		title: 'Inventory (Use)';
		borderColor: Color cyan;
		whenActivatedDo: [ :anItem | 
			self playPlayerTurn: (RgConsumeAction new
				actor: self player;
				item: anItem;
				yourself) ];
		show
]

{ #category : #accessing }
RgConsolePresenter >> terminal [

	^ terminal
]

{ #category : #private }
RgConsolePresenter >> terminalExtraSpace [
	"Extra space to put other than the game itself"

	^ 0@5
]

{ #category : #private }
RgConsolePresenter >> terminalSize [
	
	^ self gameMap size + self terminalExtraSpace
]

{ #category : #'private updating' }
RgConsolePresenter >> updateMap [

	self application defer: [ 
		terminal 
			clear; 
			hideCursor.
		self game renderOn: self.
		self updatePlayerStatus.
		self updateMessagesList ]
]

{ #category : #'private updating' }
RgConsolePresenter >> updateMessagesList [

	self withBuilderDo: [ :builder | 
		messages withIndexDo: [ :each :index |
			builder 
				foregroundColor: each second; 
				backgroundColor: each third;
				at: (22@(self gameMap size y + index)) putString: each first ] ]
]

{ #category : #'private updating' }
RgConsolePresenter >> updatePlayerStatus [
	| y |

	y := self gameMap size y.
	self withBuilderDo: [ :builder |
		builder 
			at: 1@(y + 1) 
				putBarLabel: 'HP' 
				value: self player hitPoints 
				max: self player maxHitPoints;
			foregroundColor: Color white;
			backgroundColor: Color black;
			at: 1@(y + 2) putString: ('Power: {1}' format: { self player power });
			at: 1@(y + 3) putString: ('Armor: {1}' format: { self player defense }) ].

	self flag: #TODO. "Move this to a better place."
	"Verify if he is alive"
	self player isAlive 
		ifFalse: [ self playPlayerIsDead ]
]

{ #category : #initialization }
RgConsolePresenter >> updatePresenter [

]

{ #category : #actions }
RgConsolePresenter >> wait [

	self playPlayerTurn: (RgNoAction for: self player)
]

{ #category : #'private ui' }
RgConsolePresenter >> withBuilderDo: aBlock [

	aBlock value: self newBuilder
]
