Class {
	#name : #RgApplication,
	#superclass : #SpApplication,
	#category : #'RogueLike-View'
}

{ #category : #initialization }
RgApplication >> initialize [

	super initialize.
	self useBackend: #Gtk with: RgGtkConfiguration new
]

{ #category : #'accessing resources' }
RgApplication >> resourcesDir [

	self flag: #TODO. "Refactor this into a production/development environments"
	IceRepository registry 
		detect: [ :each | each name = 'roguelike' ]
		ifFound: [ :each | ^ each location / 'resources' ].
	
	^ FileLocator imageDirectory / 'resources'
]

{ #category : #running }
RgApplication >> start [

	(RgConsolePresenter newApplication: self) openWithSpec
]

{ #category : #'accessing resources' }
RgApplication >> stylesDir [

	^ self resourcesDir / 'styles'
]
