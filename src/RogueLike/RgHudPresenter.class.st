Class {
	#name : #RgHudPresenter,
	#superclass : #RgConsolePresenter,
	#instVars : [
		'playerStatusPresenter',
		'terminalPresenter'
	],
	#category : #'RogueLike-View'
}

{ #category : #initialization }
RgHudPresenter >> initializePresenters [

	super initializePresenters.
	self layout 
		add: (SpBoxLayout newHorizontal 
				add: (playerStatusPresenter := (self instantiate: RpPlayerStatusPresenter on: self game));
				add: (terminalPresenter := (self instantiate: RpMessageLogPresenter));
				yourself)
			height: 50
]

{ #category : #initialization }
RgHudPresenter >> terminalExtraSpace [

	^ 0@0
]

{ #category : #'private updating' }
RgHudPresenter >> updateMessagesList [
]

{ #category : #'private updating' }
RgHudPresenter >> updatePlayerStatus [

	"playerStatusPresenter updatePresenter"
]

{ #category : #initialization }
RgHudPresenter >> updatePresenter [

	super updatePresenter
]
