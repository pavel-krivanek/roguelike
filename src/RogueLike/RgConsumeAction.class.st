Class {
	#name : #RgConsumeAction,
	#superclass : #RgAction,
	#instVars : [
		'item'
	],
	#category : #'RogueLike-Actions'
}

{ #category : #'instance creation' }
RgConsumeAction class >> appliesTo: toTile [
	
	^ toTile items notEmpty
]

{ #category : #'instance creation' }
RgConsumeAction class >> newActor: anActor tile: aTile [

	^ self new 
		actor: anActor;
		targetTile: aTile;
		yourself
]

{ #category : #execution }
RgConsumeAction >> execute [

	self actor doConsume: self
]

{ #category : #accessing }
RgConsumeAction >> item [

	^ item
]

{ #category : #accessing }
RgConsumeAction >> item: anObject [

	item := anObject
]
