Class {
	#name : #RgSeedMapGenerator,
	#superclass : #RgRandomMapGenerator,
	#instVars : [
		'seed',
		'random'
	],
	#category : #'RogueLike-Generator'
}

{ #category : #'instance creation' }
RgSeedMapGenerator class >> newSeed: aNumber [

	^ self new seed: aNumber
]

{ #category : #'instance creation' }
RgSeedMapGenerator class >> newTick [

	^ self newSeed: Time millisecondClockValue
]

{ #category : #private }
RgSeedMapGenerator >> random [

	^ random ifNil: [ random := Random seed: self seed ]
]

{ #category : #private }
RgSeedMapGenerator >> random: aNumberOrCollection [

	^ aNumberOrCollection atRandom: self random
]

{ #category : #accessing }
RgSeedMapGenerator >> seed [

	^ seed
]

{ #category : #accessing }
RgSeedMapGenerator >> seed: aNumber [

	seed := aNumber.
	random := nil
]
