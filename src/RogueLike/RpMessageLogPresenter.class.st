Class {
	#name : #RpMessageLogPresenter,
	#superclass : #SpPresenter,
	#category : #'RogueLike-View'
}

{ #category : #initialization }
RpMessageLogPresenter >> initializePresenters [

	self layout: (SpBoxLayout newVertical 
		add: self newTerminal;
		yourself).
		
	self addStyle: 'messageLog'
]

{ #category : #scripting }
RpMessageLogPresenter >> newTerminal [
	| presenter |

	presenter := self instantiate: VteTerminalPresenter.
	
	presenter
		contextMenu: (SpMenuPresenter new
			addItem: [ :item | item name: 'Clear'; action: [ presenter clear ] ];
			yourself);
		withHyperlinks;
		whenBuiltDo: [ :w | self prepareAsTerminal: w model ].
		
	"presenter addStyle: 'messageLog'."
	"presenter announcer 
		when: VteHyperlinkPressed 
		send: #hyperlinkPressed: 
		to: self. "
	
	^ presenter
]

{ #category : #private }
RpMessageLogPresenter >> prepareAsTerminal: aPresenter [

	"maybe move to presenter?"
	aPresenter adapter innerWidget
		scrollOnOutput: true;
		mouseAutohide: true
]
