Class {
	#name : #RgBumpAction,
	#superclass : #RgAction,
	#instVars : [
		'targetTile'
	],
	#category : #'RogueLike-Actions'
}

{ #category : #'instance creation' }
RgBumpAction class >> appliesTo: toTile [

	^ false
]

{ #category : #'instance creation' }
RgBumpAction class >> newActor: anActor tile: aTile [

	^ (self allSubclasses
		detect: [ :each | each appliesTo: aTile ]
		ifNone: [ RgMovementAction ]) new 
		actor: anActor;
		targetTile: aTile;
		yourself
]

{ #category : #accessing }
RgBumpAction >> targetTile [

	^ targetTile
]

{ #category : #accessing }
RgBumpAction >> targetTile: aTile [

	targetTile := aTile
]
