Class {
	#name : #RgHealingMusrhumConsumable,
	#superclass : #RgHealingConsumable,
	#category : #'RogueLike-Model-Items'
}

{ #category : #printing }
RgHealingMusrhumConsumable >> color [

	^ Color orange
]

{ #category : #initialization }
RgHealingMusrhumConsumable >> initialize [

	super initialize.
	self amount: 5
]

{ #category : #accessing }
RgHealingMusrhumConsumable >> name [

	^ 'Champignon de guerison'
]

{ #category : #printing }
RgHealingMusrhumConsumable >> symbol [

	^ Unicode value: 16r0B21
]
