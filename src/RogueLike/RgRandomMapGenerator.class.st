Class {
	#name : #RgRandomMapGenerator,
	#superclass : #RgMapGenerator,
	#category : #'RogueLike-Generator'
}

{ #category : #generating }
RgRandomMapGenerator >> generateEntities [

	self map rooms do: [ :each |
		(self random: self map maxMonstersPerRoom) timesRepeat: [ 
			self 
				randomPlaceEntity: self nextEntity 
				atRoom: each ] ]
]

{ #category : #generating }
RgRandomMapGenerator >> generateItems [
	
	RgItem allItems ifEmpty: [ ^ self ].
	(self random: self map maxItems) timesRepeat: [
		self 
			randomPlaceEntity: self nextItem 
			atRoom: (self random: self map rooms) ]
	
]

{ #category : #generating }
RgRandomMapGenerator >> generateMap [
	| rooms roomWidth roomHeight x y |

	self map fillWith: [ :aPosition | RgWall newPosition: aPosition ].
	rooms := OrderedCollection new.
	self map maxRooms timesRepeat: [ | room |
		roomWidth := self random: (self map minRoomSize to: self map maxRoomSize).
		roomHeight := self random: (self map minRoomSize to: self map maxRoomSize).
		x := self random: (self map size x - roomWidth).
		y := self random: (self map size y - roomHeight).
		
		room := x@y extent: (roomWidth@roomHeight).
		(rooms anySatisfy: [ :each | each intersects: room ]) ifFalse: [
			self addRoom: room.
			rooms ifNotEmpty: [ 
				self 
					addTunnelFrom: rooms last center 
					to: room center ].
			rooms add: room.
			self map addRoom: room ] ]
]

{ #category : #generating }
RgRandomMapGenerator >> generatePlayer [

	self map 
		placeEntity: self player 
		at: (self map rooms first center)
]

{ #category : #private }
RgRandomMapGenerator >> nextEntity [ 
	
	self flag: #TODO. "Refactor"
	^ (self random: 100) < 8 
		ifTrue: [ RgTroll new ]
		ifFalse: [ RgOrc new ]
]

{ #category : #private }
RgRandomMapGenerator >> nextItem [ 
	
	self flag: #TODO. "Refactor"
	^ (self random: RgItem allItems) new
]

{ #category : #private }
RgRandomMapGenerator >> random: aNumberOrCollection [

	^ aNumberOrCollection atRandom
]

{ #category : #private }
RgRandomMapGenerator >> randomPlaceEntity: anEntity atRoom: aRect [
	| x y |

	x := self random: ((aRect left + 1)  to: (aRect right - 1)). 
	y := self random: ((aRect top + 1) to: (aRect bottom - 1)).

	self map 
		placeEntity: anEntity 
		at: x@y	
]
