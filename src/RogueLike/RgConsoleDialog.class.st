"
```
(RgConsoleDialog on: aConsole)
	extent: 40@12;
	content: [ :builder | 
		builder at: 1@1 putString: 'Hello!' ];
	show
```
"
Class {
	#name : #RgConsoleDialog,
	#superclass : #RgConsoleWindow,
	#instVars : [
		'eventHandler',
		'closeAction',
		'borderDecoration'
	],
	#category : #'RogueLike-View'
}

{ #category : #'instance creation' }
RgConsoleDialog class >> new [

	self error: 'Use #on:'
]

{ #category : #'instance creation' }
RgConsoleDialog class >> on: aConsole [

	^ self basicNew
		initializeConsole: aConsole;
		yourself
]

{ #category : #initialization }
RgConsoleDialog >> borderColor: aColor [

	self ensureBorderDecoration borderColor: aColor
]

{ #category : #showing }
RgConsoleDialog >> close [

	self console popDialog: self.
	closeAction ifNotNil: [ closeAction cull: self ].
	
]

{ #category : #private }
RgConsoleDialog >> ensureBorderDecoration [

	borderDecoration ifNil: [ 
		self addDecoration: ((borderDecoration := RgWindowBorderDecoration new) beBoldBorder) ].

	^ borderDecoration
]

{ #category : #accessing }
RgConsoleDialog >> eventHandler [

	^ eventHandler ifNil: [ eventHandler := self newEventHandler ]
]

{ #category : #initialization }
RgConsoleDialog >> initialize [

	super initialize.
	self beCentered
]

{ #category : #'private factory' }
RgConsoleDialog >> newEventHandler [

	^ RgKeyboardEventHandler new 
		bindKeyCombination: Character escape asKeyCombination toAction: [ self close ];
		yourself. 
]

{ #category : #showing }
RgConsoleDialog >> show [

	super show.
	self console pushDialog: self
]

{ #category : #initialization }
RgConsoleDialog >> title: aString [

	self ensureBorderDecoration title: aString
]

{ #category : #showing }
RgConsoleDialog >> whenClosedDo: aBlock [

	closeAction := aBlock
]
