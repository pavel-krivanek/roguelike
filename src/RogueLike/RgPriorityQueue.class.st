Class {
	#name : #RgPriorityQueue,
	#superclass : #Object,
	#instVars : [
		'elements'
	],
	#category : #'RogueLike-Utils'
}

{ #category : #'accessing queue' }
RgPriorityQueue >> at: aPriority put: anObject [

	elements add: (RgPriorityElement new 
		element: anObject;
		priority: aPriority;
		yourself)
]

{ #category : #private }
RgPriorityQueue >> errorEmptyQueue [

	self error: 'this queue is empty'
]

{ #category : #'accessing queue' }
RgPriorityQueue >> get [

	self isEmpty ifTrue: [ self errorEmptyQueue ].
	^ elements removeFirst element
]

{ #category : #initialization }
RgPriorityQueue >> initialize [

	super initialize.
	elements := SortedCollection sortUsing: [ :each | each priority ] ascending
]

{ #category : #testing }
RgPriorityQueue >> isEmpty [
	
	^ elements isEmpty
]

{ #category : #accessing }
RgPriorityQueue >> prioritizedList [
	
	^ elements 
		collect: [ :each | each element ]
		as: Array
]
