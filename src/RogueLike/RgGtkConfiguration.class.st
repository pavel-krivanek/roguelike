Class {
	#name : #RgGtkConfiguration,
	#superclass : #SpGtkConfiguration,
	#category : #'RogueLike-View'
}

{ #category : #configuring }
RgGtkConfiguration >> configure: anApplication [
	
	super configure: anApplication. 
	GtkRunLoop defer: [ 
		self installStylesOn: anApplication ]
]

{ #category : #configuring }
RgGtkConfiguration >> installStylesOn: anApplication [

	self addCSSProviderFromReference: anApplication stylesDir / 'general.css'.	

]
