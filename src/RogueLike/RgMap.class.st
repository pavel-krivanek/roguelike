Class {
	#name : #RgMap,
	#superclass : #Object,
	#instVars : [
		'game',
		'matrix',
		'size',
		'rooms',
		'visibleTiles',
		'exploredTiles',
		'entities',
		'pathFinder'
	],
	#category : #'RogueLike-Model'
}

{ #category : #accessing }
RgMap class >> defaultMapSize [

	^ 140@40
]

{ #category : #accessing }
RgMap class >> fovRadius [

	^ 10
]

{ #category : #accessing }
RgMap class >> maxItems [

	^ 5
]

{ #category : #accessing }
RgMap class >> maxMonstersPerRoom [
	
	^ 2
]

{ #category : #accessing }
RgMap class >> maxRoomSize [ 
	
	^ 10
]

{ #category : #accessing }
RgMap class >> maxRooms [

	^ 50
]

{ #category : #accessing }
RgMap class >> minRoomSize [ 
	
	^ 6
]

{ #category : #'instance creation' }
RgMap class >> new [

	self error: 'Use #newGame:'
]

{ #category : #'instance creation' }
RgMap class >> newGame: aGame [

	^ self basicNew 
		initializeGame: aGame;
		yourself
]

{ #category : #accessing }
RgMap >> add: aTile [

	aTile map: self.
	matrix 
		at: aTile position y
		at: aTile position x 
		put: aTile
]

{ #category : #accessing }
RgMap >> addAll: aCollection [

	aCollection do: [ :each | self add: each ]	
]

{ #category : #generating }
RgMap >> addRoom: aRectangle [

	rooms add: aRectangle
]

{ #category : #testing }
RgMap >> canMoveToPosition: aPoint [

	^ (self isInBounds: aPoint)
		and: [ (self tileAt: aPoint) isWalkable ]
]

{ #category : #private }
RgMap >> circularRayTo: points origin: origin dest: dest radiusSquared: radiusSquared [
	| rayPoints |

	rayPoints := RgUtils bresenhamFrom: origin to: dest.
	rayPoints := rayPoints select: [ :each | self computeBounds containsPoint: each ].
	rayPoints ifEmpty: [ ^ self ].
	
	rayPoints do: [ :each | 
		"Validate point is still on range"
		(((each x - origin x)**2) + ((each y - origin y)**2)) > radiusSquared 
			ifTrue: [ ^ self ].
		"add it"
		points add: each.
		"break if visibility is blocked"
		(self tileAt: each) isTransparent 
			ifFalse: [ ^ self ]  ]
]

{ #category : #private }
RgMap >> computeBounds [
	
	^ 1@1 corner: self computeSize
]

{ #category : #private }
RgMap >> computeCircularFov: pov radius: radius [
	| xmin ymin xmax ymax points radiusSquared |
	
	points := OrderedCollection new.
	xmin := 0.
	ymin := 0.
	xmax := self computeSize x.
	ymax := self computeSize y. 
	
	radius > 0 ifTrue: [ 
		xmin := xmin max: (pov x - radius).
		ymin := ymin max: (pov y - radius). 
		xmax := xmax min: (pov x + radius + 1).
		ymax := ymax min: (pov y + radius + 1) ].
	
	points add: pov.
	
	radiusSquared := radius * radius.
	xmin to: (xmax - 1) do: [ :x | 
		self circularRayTo: points origin: pov dest: x@ymin radiusSquared: radiusSquared ].
	(ymin + 1) to: (ymax - 1) do: [ :y | 
		self circularRayTo: points origin: pov dest: (xmax - 1)@y radiusSquared: radiusSquared ].	
	(xmax - 2) to: xmin by: -1 do: [ :x | 
		self circularRayTo: points origin: pov dest: x@(ymax - 1) radiusSquared: radiusSquared ].	
	(ymax - 2) to: (ymin - 1) by: -1 do: [ :y | 
		self circularRayTo: points origin: pov dest: xmin@y radiusSquared: radiusSquared ].	

	^ points
]

{ #category : #private }
RgMap >> computeSize [

	^ self size + (1@1)
]

{ #category : #enumerating }
RgMap >> detect: aBlock [
	
	^ matrix detect: aBlock
]

{ #category : #enumerating }
RgMap >> do: aBlock [ 

	^ matrix do: aBlock
]

{ #category : #'accessing pathfind' }
RgMap >> edgesOf: aTile [
	"An ugly algorithm, but it should be optimal"
	| pos tile edgePos |
	
	^ Array new: 8 streamContents: [ :stream | 
		pos := aTile position.
		pos x - 1 to: pos x + 1 do: [ :x |
			pos y - 1 to: pos y + 1 do: [ :y |
				edgePos := x@y. 
				((self isInBounds: edgePos) 
					and: [ pos ~= edgePos ]) 
					ifTrue: [
						tile := self tileAt: edgePos.
						tile isWalkable 
							ifTrue: [ stream nextPut: tile ] ] ] ] ]
]

{ #category : #accessing }
RgMap >> entities [

	^ entities
]

{ #category : #accessing }
RgMap >> fillWith: aBlock [

	matrix := Array2D 
		rows: size y
		columns: size x 
		tabulate: [ :y :x | 
			(aBlock value: x@y)
				map: self;
				yourself ]
]

{ #category : #accessing }
RgMap >> fovRadius [

	^ self class fovRadius
]

{ #category : #accessing }
RgMap >> game [

	^ game
]

{ #category : #generating }
RgMap >> generateWith: aGenerator [

	aGenerator 
		map: self;
		generate
]

{ #category : #'accessing pathfind' }
RgMap >> graph [
	
	^ matrix
		select: [ :each | each isWalkable ]
		thenCollect: [ :each | each -> (self edgesOf: each) ]
]

{ #category : #initialization }
RgMap >> initializeGame: aGame [

	self initialize.
	game := aGame.
	entities := OrderedCollection new.
	rooms := OrderedCollection new.
	visibleTiles := OrderedCollection new.
	exploredTiles := Set new.
	size := self class defaultMapSize.
	
	self generateWith: RgSeedMapGenerator newTick
]

{ #category : #testing }
RgMap >> isInBounds: aPoint [
		
	^ aPoint >= (1@1)
		and: [ aPoint <= self size ]
]

{ #category : #testing }
RgMap >> isVisible: aTile [
	
	^ visibleTiles includes: aTile
]

{ #category : #accessing }
RgMap >> maxItems [

	^ self class maxItems
]

{ #category : #accessing }
RgMap >> maxMonstersPerRoom [

	^ self class maxMonstersPerRoom
]

{ #category : #accessing }
RgMap >> maxRoomSize [

	^ self class maxRoomSize
]

{ #category : #accessing }
RgMap >> maxRooms [

	^ self class maxRooms
]

{ #category : #accessing }
RgMap >> minRoomSize [

	^ self class minRoomSize
]

{ #category : #'accessing pathfind' }
RgMap >> newPathFinder [
	| graph |

	graph := self pathGraph.
	^ RgAStar new 
		nodes: graph keys;
		edges: graph associations from: #key toAll: #value;
		diagonalWeight: 1;
		cardinalWeight: 0;
		yourself
]

{ #category : #'accessing pathfind' }
RgMap >> pathFinder [
	
	^ pathFinder ifNil: [ pathFinder := self newPathFinder ]
]

{ #category : #'accessing pathfind' }
RgMap >> pathGraph [
	"to be used for pathfinder algorithms "
	| pathGraph |
	
	pathGraph := OrderedDictionary new: matrix privateContents size.
	matrix privateContents
		select: [ :each | each isWalkable ]
		thenDo: [ :each | pathGraph at: each put: (self edgesOf: each) ].
	^ pathGraph
]

{ #category : #generating }
RgMap >> placeEntity: anEntity at: aPosition [

	(entities includes: anEntity)
		ifFalse: [ entities add: anEntity ].
	(self tileAt: aPosition) addEntity: anEntity
]

{ #category : #accessing }
RgMap >> player [

	^ self game player
]

{ #category : #generating }
RgMap >> removeEntity: anEntity [

	anEntity tile removeEntity: anEntity.
	entities remove: anEntity.
]

{ #category : #removing }
RgMap >> removeEntity: anEntity from: aPoint [ 
	
	self entities remove: anEntity.
	(self tileAt: aPoint) removeEntity: anEntity
]

{ #category : #rendering }
RgMap >> renderOn: aConsole [

	(exploredTiles \ visibleTiles) do: [ :each | each renderExploredOn: aConsole ].
	visibleTiles do: [ :each | each renderVisibleOn: aConsole ].

]

{ #category : #accessing }
RgMap >> rooms [

	^ rooms
]

{ #category : #enumerating }
RgMap >> select: aBlock [
	
	^ matrix select: aBlock
]

{ #category : #enumerating }
RgMap >> select: aBlock thenDo: doBlock [

	^ matrix select: aBlock thenDo: doBlock
]

{ #category : #accessing }
RgMap >> size [

	^ size
]

{ #category : #accessing }
RgMap >> tileAt: aPoint [

	^ matrix at: aPoint y at: aPoint x
]

{ #category : #rendering }
RgMap >> updateFov: aPlayer [

	exploredTiles addAll: visibleTiles.
	visibleTiles removeAll.

	(self computeCircularFov: aPlayer position radius: self fovRadius)
		do: [ :each  | visibleTiles add: (self tileAt: each) ]
]

{ #category : #enumerating }
RgMap >> withIndicesDo: aBlock [
	
	^ matrix withIndicesDo: aBlock
]
