Class {
	#name : #RgWindowBorderDecoration,
	#superclass : #RgWindowDecoration,
	#instVars : [
		'borderCharacters',
		'borderColor',
		'title'
	],
	#category : #'RogueLike-View'
}

{ #category : #initialization }
RgWindowBorderDecoration class >> defaultBorderColor [

	^ Color white
]

{ #category : #accessing }
RgWindowBorderDecoration >> beBoldBorder [

	borderCharacters := SmallDictionary newFromPairs: {  
		#topLeft. 		Unicode value: 16r250f.
		#topRight. 		Unicode value: 16r2513.
		#top. 				Unicode value: 16r2501.
		#bottom. 			Unicode value: 16r2501.
		#bottomLeft. 	Unicode value: 16r2517.
		#bottomRight. 	Unicode value: 16r251b.
		#left. 			Unicode value: 16r2503.
		#right. 			Unicode value: 16r2503.
		"title"
		#titleLeft. 		Unicode value: 16r252b.
		#titleRight. 	Unicode value: 16r2523.
	}
]

{ #category : #accessing }
RgWindowBorderDecoration >> beSimpleBorder [

	borderCharacters := SmallDictionary newFromPairs: {  
		#topLeft. 		Unicode value: 16r250c.
		#topRight. 		Unicode value: 16r2510.
		#top. 				Unicode value: 16r2500.
		#bottom. 			Unicode value: 16r2500.
		#bottomLeft. 	Unicode value: 16r2514.
		#bottomRight. 	Unicode value: 16r2518.
		#left. 			Unicode value: 16r2502.
		#right. 			Unicode value: 16r2502.
		"title"
		#titleLeft. 		Unicode value: 16r2524.
		#titleRight. 	Unicode value: 16r252c.
	}
]

{ #category : #private }
RgWindowBorderDecoration >> borderCharacterAt: aSymbol [

	^ borderCharacters at: aSymbol
]

{ #category : #accessing }
RgWindowBorderDecoration >> borderColor [

	^ borderColor ifNil: [ self class defaultBorderColor ]
]

{ #category : #accessing }
RgWindowBorderDecoration >> borderColor: aColor [

	borderColor := aColor
]

{ #category : #drawing }
RgWindowBorderDecoration >> drawBorderOn: aBuilder [
	| extent |

	aBuilder foregroundColor: self borderColor.
	extent := self window extent.
	aBuilder at: 0@0 putCharacter: (self borderCharacterAt: #topLeft).
	aBuilder at: (extent x)@0 putCharacter: (self borderCharacterAt: #topRight).
	aBuilder at: 0@(extent y) putCharacter: (self borderCharacterAt: #bottomLeft).
	aBuilder at: (extent x)@(extent y) putCharacter: (self borderCharacterAt: #bottomRight).
	1 to: (extent x - 1) do: [ :x |
		aBuilder at: x@0 putCharacter: (self borderCharacterAt: #top).
		aBuilder at: x@(extent y) putCharacter: (self borderCharacterAt: #bottom) ]. 
	1 to: (extent y - 1) do: [ :y |
		aBuilder at: 0@y putCharacter: (self borderCharacterAt: #left).
		aBuilder at: (extent x)@y putCharacter: (self borderCharacterAt: #right) ].
 
]

{ #category : #drawing }
RgWindowBorderDecoration >> drawOn: aBuilder [

	self drawBorderOn: aBuilder.
	self drawTitleOn: aBuilder.
	
	aBuilder translateTo: aBuilder translation + 1
]

{ #category : #drawing }
RgWindowBorderDecoration >> drawTitleOn: aBuilder [
	
	self title ifNil: [ ^ self ].
	
	aBuilder 
		at: (self window extent x - (self title size + 4) / 2) asInteger @ 0
		putString: (String streamContents: [ :stream | 
			stream nextPut: (self borderCharacterAt: #titleLeft). 
			stream space.
			stream nextPutAll: self title.
			stream space. 
			stream nextPut: (self borderCharacterAt: #titleRight) ])
]

{ #category : #initialization }
RgWindowBorderDecoration >> initialize [

	super initialize.
	self beSimpleBorder. 
]

{ #category : #accessing }
RgWindowBorderDecoration >> title [

	^ title
]

{ #category : #accessing }
RgWindowBorderDecoration >> title: aString [

	title := aString
]
