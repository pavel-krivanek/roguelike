Class {
	#name : #RgAStarTest,
	#superclass : #TestCase,
	#instVars : [
		'star'
	],
	#category : #'RogueLike-Utils'
}

{ #category : #accessing }
RgAStarTest >> aseCircuit: builder [
	| nodes edges |
	
	
	nodes := $a to: $h.
	edges := #(($a $b) ($b $a) ($b $c) ($b $d) ($c $d) ($c $f) ($d $b) ($d $e) ($e $a) ($f $g) ($g $h) ($h $g)).


	nodes := nodes collect: [ :each | self charToFloor: each ].
	edges := edges collect: [ :each | { self charToFloor: each first. self charToFloor: each second } ].

	builder nodes: nodes.
	builder edges: edges from: #first to: #second.
	
	^ builder	
]

{ #category : #accessing }
RgAStarTest >> charToFloor: aCharacter [

	^ RgFloor newPosition: (aCharacter asciiValue - 96)@1
]

{ #category : #running }
RgAStarTest >> setUp [

	super setUp.
	star := RgAStar new
]

{ #category : #tests }
RgAStarTest >> testAseBasicCircuit [

	self aseCircuit: star.

	self assert: 1 equals: (star runFrom: (self charToFloor: $a) to: (self charToFloor: $b)).
	star reset.
	self assert: 3 equals: (star runFrom: (self charToFloor: $a) to: (self charToFloor: $e)).
	star reset.
	self assert: 2 equals: (star runFrom: (self charToFloor: $c) to: (self charToFloor: $b)).
	star reset.
	self assert: 5 equals: (star runFrom: (self charToFloor: $a) to: (self charToFloor: $h)).
	star reset.
	self assert: Float infinity equals: (star runFrom: (self charToFloor: $h) to: (self charToFloor: $a))
]

{ #category : #tests }
RgAStarTest >> testAseBasicCircuitBacktrack [

	self assert: (#($a $b) hasEqualElements: (star runFrom: $a to: $b; backtrack)).
	star reset.
	self assert: (#($a $b $d $e) hasEqualElements: (star runFrom: $a to: $e; backtrack)).
	star reset.
	self assert: (#($c $d $b) hasEqualElements: (star runFrom: $c to: $b; backtrack)).
	star reset.
	self assert: (#($a $b $c $f $g $h) hasEqualElements: (star)).
	star reset.
	self assert: (#() hasEqualElements: (star runFrom: $h to: $a; backtrack))
]
