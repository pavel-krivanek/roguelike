Class {
	#name : #RgMovementAction,
	#superclass : #RgBumpAction,
	#category : #'RogueLike-Actions'
}

{ #category : #'instance creation' }
RgMovementAction class >> newActor: anActor tile: aTile [

	^ self new 
		actor: anActor;
		targetTile: aTile;
		yourself
]

{ #category : #execution }
RgMovementAction >> execute [

	self actor doMove: self
]
