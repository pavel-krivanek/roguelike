Class {
	#name : #RgTile,
	#superclass : #Object,
	#instVars : [
		'map',
		'position'
	],
	#category : #'RogueLike-Model'
}

{ #category : #'instance creation' }
RgTile class >> newPosition: aPoint [

	^ self new 
		position: aPoint;
		yourself
]

{ #category : #comparing }
RgTile >> = other [

	^ self species = other species 
		and: [ self position = other position ]
]

{ #category : #testing }
RgTile >> canAttackPosition [
	
	^ false
]

{ #category : #accessing }
RgTile >> darkBackgroundColor [
	"Color blueColors" 
	^ self subclassResponsibility
]

{ #category : #accessing }
RgTile >> darkCharacter [

	^ Character space
]

{ #category : #accessing }
RgTile >> darkForegroundColor [

	^ Color white
]

{ #category : #comparing }
RgTile >> hash [

	^ self class hash bitXor: self position hash
]

{ #category : #testing }
RgTile >> isTransparent [

	^ self subclassResponsibility
]

{ #category : #testing }
RgTile >> isVisible [

	^ self map isVisible: self
]

{ #category : #testing }
RgTile >> isWalkable [

	^ false
]

{ #category : #accessing }
RgTile >> lightBackgroundColor [

	^ Color named: #coolBlue "#antiqueWhite3"
]

{ #category : #accessing }
RgTile >> lightCharacter [

	^ Character space
]

{ #category : #accessing }
RgTile >> lightForegroundColor [

	^ Color white
]

{ #category : #accessing }
RgTile >> map [

	^ map
]

{ #category : #accessing }
RgTile >> map: aMap [

	map := aMap
]

{ #category : #accessing }
RgTile >> position [

	^ position
]

{ #category : #accessing }
RgTile >> position: aPoint [

	position := aPoint
]

{ #category : #printing }
RgTile >> printOn: stream [

	super printOn: stream.
	stream << '(' << self position asString << ')'
]

{ #category : #rendering }
RgTile >> renderExploredOn: aConsole [
	
	^ aConsole renderExploredTile: self
]

{ #category : #rendering }
RgTile >> renderOn: aConsole [
	
	^ aConsole renderTile: self
]

{ #category : #rendering }
RgTile >> renderVisibleOn: aConsole [
	
	^ aConsole renderVisibleTile: self
]

{ #category : #printing }
RgTile >> symbol [

	^ self darkCharacter
]
