Class {
	#name : #RgPlayer,
	#superclass : #RgActor,
	#instVars : [
		'inventory'
	],
	#category : #'RogueLike-Model-Actors'
}

{ #category : #printing }
RgPlayer >> color [

	^ Color white
]

{ #category : #accessing }
RgPlayer >> defense [

	^ 2
]

{ #category : #actions }
RgPlayer >> doConsume: anAction [

	(anAction item 
		newAction: self)
		executeOn: anAction console.
		
	anAction item isConsumed 
		ifTrue: [ self inventory removeItem: anAction item ]
]

{ #category : #actions }
RgPlayer >> doDropItem: anAction [

	inventory dropItem: anAction item.
	anAction addMessage: ('You dropped the {1}' format: { anAction item name }) 
	
]

{ #category : #actions }
RgPlayer >> doPickItem: anAction [

	inventory pickItem: anAction item.
	anAction addMessage: ('You picked up the {1}' format: { anAction item name }) 
		
]

{ #category : #initialization }
RgPlayer >> initialize [

	super initialize.
	inventory := RgInventory on: self
]

{ #category : #accessing }
RgPlayer >> inventory [

	^ inventory
]

{ #category : #testing }
RgPlayer >> isBlocking [

	^ false
]

{ #category : #testing }
RgPlayer >> isHostileTo: anActor [

	^ anActor isHostile
]

{ #category : #testing }
RgPlayer >> isPlayer [

	^ true
]

{ #category : #accessing }
RgPlayer >> maxHitPoints [

	^ 30
]

{ #category : #accessing }
RgPlayer >> name [

	^ 'Player'
]

{ #category : #accessing }
RgPlayer >> power [

	^ 5
]

{ #category : #printing }
RgPlayer >> symbol [

	^ $@
]
