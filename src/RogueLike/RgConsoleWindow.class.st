Class {
	#name : #RgConsoleWindow,
	#superclass : #Object,
	#instVars : [
		'position',
		'extent',
		'console',
		'contentBlock',
		'centered',
		'decorations',
		'backgroundColor'
	],
	#category : #'RogueLike-View'
}

{ #category : #accessing }
RgConsoleWindow class >> defaultBackgroundColor [
	
	^ Color black
]

{ #category : #accessing }
RgConsoleWindow class >> defaultExtent [

	^ 40@20
]

{ #category : #accessing }
RgConsoleWindow >> addBorder [

	self addDecoration: RgWindowBorderDecoration new
]

{ #category : #accessing }
RgConsoleWindow >> addDecoration: aDecoration [

	decorations ifNil: [ decorations := #() ].
	aDecoration window: self.
	decorations := decorations copyWith: aDecoration
]

{ #category : #accessing }
RgConsoleWindow >> backgroundColor [

	^ backgroundColor ifNil: [ self class defaultBackgroundColor ]
]

{ #category : #accessing }
RgConsoleWindow >> backgroundColor: aColor [

	backgroundColor := aColor
]

{ #category : #accessing }
RgConsoleWindow >> beCentered [

	centered := true
]

{ #category : #accessing }
RgConsoleWindow >> beNotCentered [

	centered := false
]

{ #category : #drawing }
RgConsoleWindow >> clear: builder [
	| string |
	
	builder backgroundColor: self backgroundColor.
	string := String new: self extent x withAll: Character space.
	0 to: (self extent y - 1) do: [ :y | 
		builder at: 0@y putString: string ]
]

{ #category : #accessing }
RgConsoleWindow >> console [

	^ console
]

{ #category : #accessing }
RgConsoleWindow >> content [
	
	^ contentBlock
]

{ #category : #accessing }
RgConsoleWindow >> content: aBlock [

	contentBlock := aBlock
]

{ #category : #drawing }
RgConsoleWindow >> drawContentOn: builder [

	self content value: builder
]

{ #category : #drawing }
RgConsoleWindow >> drawDecorationsOn: builder [
	
	decorations ifNil: [ ^ self ].
	decorations do: [ :each | 
		each drawOn: builder ]
]

{ #category : #drawing }
RgConsoleWindow >> drawOn: builder [
	
	self clear: builder.
	self drawDecorationsOn: builder.
	self drawContentOn: builder
]

{ #category : #accessing }
RgConsoleWindow >> extent [

	^ extent ifNil: [ self class defaultExtent ]
]

{ #category : #accessing }
RgConsoleWindow >> extent: aPoint [

	extent := aPoint
]

{ #category : #initialization }
RgConsoleWindow >> initialize [

	super initialize.
	centered := false
]

{ #category : #initialization }
RgConsoleWindow >> initializeConsole: aConsole [

	self initialize.
	console := aConsole
]

{ #category : #testing }
RgConsoleWindow >> isCentered [
	
	^ centered
]

{ #category : #accessing }
RgConsoleWindow >> position [

	^ position ifNil: [ 0@0 ]
]

{ #category : #accessing }
RgConsoleWindow >> position: aPoint [

	position := aPoint
]

{ #category : #accessing }
RgConsoleWindow >> show [

	self console drawWindow: self
]
