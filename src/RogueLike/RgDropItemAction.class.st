Class {
	#name : #RgDropItemAction,
	#superclass : #RgAction,
	#instVars : [
		'item'
	],
	#category : #'RogueLike-Actions'
}

{ #category : #execution }
RgDropItemAction >> execute [

	self actor doDropItem: self
]

{ #category : #accessing }
RgDropItemAction >> item [

	^ item
]

{ #category : #accessing }
RgDropItemAction >> item: anObject [

	item := anObject
]
